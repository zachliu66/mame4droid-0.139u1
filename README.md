Please using pre ndk 17 version to compile! I have using ndk 16 to compile successfully.

=======================================================================
MAME4droid (0.139u1) is developed by David Valdeita (Seleuco) as a port of the MAME 0.139 emulator by Nicola Salmoria and the MAME team. It emulates arcade games supported by this version of MAME which includes over 8000 different ROMs.

* MAME4droid is an EMULATOR and DOES NOT INCLUDE ROMS OR COPYRIGHTED MATERIAL OF ANY KIND.

This version of MAME4droid was designed to be used with dual-core Android devices because it is based on the PC MAME version which requires higher specifications than the older versions.

Even with a high end device, do not expect "modern" arcade games from the 90s and beyond to necessarily work at full speed or compatibility. Some games like Outrun and the Mortal Kombat series are badly optimized and require at least a 1.5ghz dual core device.

With over 8000 games supported, some games will run better than others; some games may not run at all. It is impossible to support such a vast number of titles, so please do not email the developer asking for support for a specific game.

After installing, place your MAME-titled zipped roms in /sdcard/MAME4droid/roms folder.

This MAME4droid version uses only '0.139' romset.

Official web page for news, source code & additional information:

https://sourceforge.net/projects/mame4droid/
